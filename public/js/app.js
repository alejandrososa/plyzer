// TODO Your code goes here.
(function ($) {
    "use strict";

    var Searcher = function () {
        // Create reference to this instance
        var o = this;
        // Initialize app when document is ready
        $(document).ready(function () {
            o.inicializar();
        });

    };

    /**
     * var self settings
     * @type {{}}
     */
    var self = {};

    /**
     * var p prototype
     * @type {Searcher}
     */
    var p = Searcher.prototype;


    // =========================================================================
    // MEMBERS
    // =========================================================================
    self = {
        container : '.container-fluid',
        rowResponse : '.row.response',
        alertError : '.not-found-product-alert',
        form : '#search-form',
        btnSubmit : '#search-form button[type="submit"]',
        txtReference : '#search-form input[name="reference"]',
        uriUrl : 'getInfo.php?reference='
    };

    // =========================================================================
    // INIT
    // =========================================================================
    p.inicializar = function () {
        p._submitForm();
    };


    // =========================================================================
    // METHOD
    // =========================================================================
    p._submitForm = function() {
        var o = this;
        var $container = $(self.container);
        var $form = $(self.form);
        var $btn = $(self.btnSubmit);
        var $txt = $(self.txtReference);

        if ($form.length > 0 && $btn.length > 0 ) {
            $form.on('submit', function (e) {
                e.preventDefault();
                o._animationButton($btn, true);

                $.ajax({
                    url: self.uriUrl + $txt.val(),
                    type: "GET",
                    dataType: "json",
                    error: function(jqXHR, textStatus, errorThrown) {
                        var err = JSON.parse(jqXHR.responseText);

                        o._removeAlert(self.alertError);
                        $container.find(self.rowResponse).detach();
                        $container.append(p._templateAlertError(err.message));
                        console.warn(err.message);
                    },
                    success: function(data, textStatus, jqXHR) {
                        var product = jQuery.parseJSON(JSON.stringify(data));

                        o._removeAlert(self.alertError);
                        $container.find(self.rowResponse).detach();
                        $container.append(p._templateProductInfo(product));
                        console.info(product);
                    }
                });

                o._animationButton($btn, false);
            });
        }
    };

    p._templateProductInfo = function(product){
        return '<div class="row response animate">' +
            '            <div class="col-sm-8 col-md-offset-2">' +
            '            <div class="well well-lg">' +
            '               <strong>Referencia:</strong> ' + product.reference + '<br>' +
            '               <strong>Nombre:</strong> ' + product.name + '<br>' +
            '               <strong>Precio:</strong> ' + product.best_price + '<br>' +
            '               <strong>Establecimiento:</strong> ' + product.shop + '<br>' +
            '               <strong>Ahorro:</strong> ' + product.savings +
            '            </div>' +
            '</div></div>';
    };

    p._templateAlertError = function(message) {
        return '<div class="row not-found-product-alert"><div class="col-sm-8 col-md-offset-2">' +
            '<div class="alert alert-warning alert-dismissible animate" role="alert">\n' +
            '  <button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
            '       <span aria-hidden="true">&times;</span>' +
            '   </button>\n' +
            '  <strong>Oppss!</strong> '+ message +'.' +
            '</div>' +
            '</div>' +
            '</div>';
    };

    p._removeAlert = function(id){
        var row = $(id);
        var alert = row.find('.alert');
        if(alert.length > 0){
            alert.alert('close');
        }
        row.detach();
    };

    p._animationButton = function($btn, $show){
        var o = this;

        if ($btn.length > 0 ) {
            $btn.on('click', function (e) {
                var $el = $(this);
                if ($show) {
                    $el.button('loading');

                } else {
                    $el.button('reset');
                }
            })
        }
    };

    // =========================================================================
    // DEFINE NAMESPACE
    // =========================================================================

    window.app = window.app || {};
    window.app.Searcher = new Searcher;
}(jQuery)); // pass in (jQuery):