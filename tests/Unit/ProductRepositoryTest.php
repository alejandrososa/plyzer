<?php
/**
 * demo-search, Created by PhpStorm.
 * @author: Alejandro Sosa <alesjohnson@hotmail.com>
 * @copyright Copyright (c) 2018, 7/6/18 22:15
 */

namespace Tests\Unit;

use Domain\Product\Model\Product;
use Infrastructure\Product\Factory\ProductFactory;
use Infrastructure\Product\Repository\ProductCollection;
use Infrastructure\Product\Repository\ProductRepository;
use PHPUnit\Framework\TestCase;

class ProductRepositoryTest extends TestCase
{
    /**
     * @var ProductRepository
     */
    private $repository;

    public function setUp()
    {
        $product1 = ProductFactory::create('901001', 'Pasta de dientes', 1.5, 'Promofarma');
        $product2 = ProductFactory::create('901001', 'Pasta de dientes', 2.5, 'Farmacia Martorell');
        $product3 = ProductFactory::create('901001', 'Pasta de dientes', 2.15, 'Missfarma');
        $product4 = ProductFactory::create('901001', 'Pasta de dientes', 3, 'Farmacia Orjales');
        $product5 = ProductFactory::create('901002', 'Jarabe para la tos', 3.6, 'Farmacia Vence');
        $product6 = ProductFactory::create('901003', 'Pack pañales', 16.25, 'Farmasky');
        $product7 = ProductFactory::create('901004', 'Crema solar', 7.6, 'Promofarma');

        $collection = new ProductCollection();
        $collection->addProduct($product1);
        $collection->addProduct($product2);
        $collection->addProduct($product3);
        $collection->addProduct($product4);
        $collection->addProduct($product5);
        $collection->addProduct($product6);
        $collection->addProduct($product7);

        $this->repository = new ProductRepository($collection);
    }

    public function specifications()
    {
        yield ['901001', ProductRepository::DESC, 3, 1.5];
        yield ['901001', ProductRepository::ASC, 1.5, 3];
    }

    /**
     * @test
     */
    public function canFindProductsByReference()
    {
        $result = $this->repository->findAllByReference('901001');

        $this->assertNotEmpty($result);
        $this->assertCount(4, $result);
    }

    /**
     * @test
     * @dataProvider specifications
     */
    public function canFindProductsByReferenceAndOrderByPriceDesc($reference, $order, $expensive, $cheap)
    {
        $result = $this->repository->findAllByReference($reference, $order);
        /** @var Product $productCheap */
        /** @var Product $productExpensive */
        $productExpensive = current($result);
        $productCheap = end($result);

        $this->assertNotEmpty($result);
        $this->assertEquals($cheap, $productCheap->price()->amount());
        $this->assertEquals($expensive, $productExpensive->price()->amount());
    }

    /**
     * @test
     * @dataProvider specifications
     */
    public function canFindProductsByReferenceAndOrderByPrice($reference, $order, $expensive, $cheap)
    {
        $result = $this->repository->findAllByReference($reference, $order);
        /** @var Product $productCheap */
        /** @var Product $productExpensive */
        $productExpensive = current($result);
        $productCheap = end($result);

        $this->assertNotEmpty($result);
        $this->assertEquals($cheap, $productCheap->price()->amount());
        $this->assertEquals($expensive, $productExpensive->price()->amount());
    }

    /**
     * @test
     */
    public function whenItDoesNotFindProductsByReferenceReturnsEmptyArray()
    {
        $result = $this->repository->findAllByReference('901005');

        $this->assertEmpty($result);
        $this->assertCount(0, $result);
    }
}
