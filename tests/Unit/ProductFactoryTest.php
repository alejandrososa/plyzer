<?php
/**
 * demo-search, Created by PhpStorm.
 * @author: Alejandro Sosa <alesjohnson@hotmail.com>
 * @copyright Copyright (c) 2018, 7/6/18 20:24
 */

namespace Tests\Unit;

use Domain\Product\Model\Product;
use Infrastructure\Product\Factory\ProductFactory;
use PHPUnit\Framework\TestCase;

class ProductFactoryTest extends TestCase
{
    private $product = [];

    public function setUp()
    {
        $this->product = [
            'reference' => '901001',
            'name' => 'Pasta de dientes',
            'price' => 1.5,
            'shop' => 'Promofarma'
        ];
    }

    /**
     * @test
     */
    public function canCreateProduct()
    {
        $product = ProductFactory::create(
            $this->product['reference'],
            $this->product['name'],
            $this->product['price'],
            $this->product['shop']
        );

        $this->assertInstanceOf(Product::class, $product);
    }
}
