<?php
/**
 * demo-search, Created by PhpStorm.
 * @author: Alejandro Sosa <alesjohnson@hotmail.com>
 * @copyright Copyright (c) 2018, 7/6/18 12:40
 */

namespace Tests\Unit;

use Infrastructure\Product\Utils\ResponseJson;
use PHPUnit\Framework\TestCase;

class ResponseJsonTest extends TestCase
{
    public function data()
    {
        yield [['Pasta de dientes',   '901001', 1.50, 'Promofarma', 0.5], 200, 200];
        yield [['Jarabe para la tos', '901002', 2.52, 'Openfarma',  0.3], 404, 404];
        yield [['Pack pañales',       '901003', 9.75, 'Openfarma',  0.4], 405, 405];
        yield [['Crema solar',        '901004', 6.08, 'Openfarma',  0.2], 500, 500];
    }

    /**
     * @test
     * @runInSeparateProcess
     * @dataProvider data
     */
    public function itShouldReturnCorrectRequestStatusCode($data, $statusCode, $expectedStatusCode)
    {
        $response = new ResponseJson($data, $statusCode);
        $response->send();

        $this->assertSame($expectedStatusCode, http_response_code());
    }

    /**
     * @test
     * @runInSeparateProcess
     * @dataProvider data
     */
    public function itShouldReturnCorrectResponse($data, $statusCode, $expectedStatusCode)
    {
        $response = new ResponseJson($data, $statusCode);

        $expectedStringResponse = json_encode($data);

        $this->assertSame($expectedStringResponse, $response->send());
    }
}
