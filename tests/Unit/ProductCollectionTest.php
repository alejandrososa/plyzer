<?php
/**
 * demo-search, Created by PhpStorm.
 * @author: Alejandro Sosa <alesjohnson@hotmail.com>
 * @copyright Copyright (c) 2018, 7/6/18 20:50
 */

namespace Tests\Unit;

use Infrastructure\Product\Factory\ProductFactory;
use Infrastructure\Product\Repository\ProductCollection;
use PHPUnit\Framework\TestCase;

class ProductCollectionTest extends TestCase
{
    /**
     * @test
     */
    public function canIterateOverProductCollection()
    {
        $product1 = ProductFactory::create('901001', 'Pasta de dientes', 1.5, 'Promofarma');
        $product2 = ProductFactory::create('901002', 'Jarabe para la tos', 3.6, 'Farmacia Vence');
        $product3 = ProductFactory::create('901003', 'Pack pañales', 16.25, 'Farmasky');
        $product4 = ProductFactory::create('901004', 'Crema solar', 7.6, 'Promofarma');

        $collection = new ProductCollection();
        $collection->addProduct($product1);
        $collection->addProduct($product2);
        $collection->addProduct($product3);
        $collection->addProduct($product4);

        $products = [];

        foreach ($collection as $product) {
            $products[] = $product->name();
        }

        $expectedData = ['Pasta de dientes','Jarabe para la tos','Pack pañales','Crema solar'];

        $this->assertEquals($expectedData, $products);
    }

    /**
     * @test
     */
    public function canIterateOverProductCollectionAfterRemovingProduct()
    {
        $product1 = ProductFactory::create('901003', 'Pack pañales', 16.25, 'Farmasky');
        $product2 = ProductFactory::create('901001', 'Pasta de dientes', 1.5, 'Promofarma');

        $collection = new ProductCollection();
        $collection->addProduct($product1);
        $collection->addProduct($product2);
        $collection->removeProduct($product1);

        $products = [];
        foreach ($collection as $product) {
            $products[] = $product->name();
        }

        $this->assertEquals(['Pasta de dientes'], $products);
    }

    public function testCanAddProductToList()
    {
        $product = ProductFactory::create('901003', 'Pack pañales', 16.25, 'Farmasky');

        $collection = new ProductCollection();
        $collection->addProduct($product);

        $this->assertCount(1, $collection);
    }

    public function testCanRemoveProductFromList()
    {
        $product = ProductFactory::create('901002', 'Jarabe para la tos', 3.6, 'Farmacia Vence');

        $collection = new ProductCollection();
        $collection->addProduct($product);
        $collection->removeProduct($product);

        $this->assertCount(0, $collection);
    }
}
