<?php
/**
 * demo-search, Created by PhpStorm.
 * @author: Alejandro Sosa <alesjohnson@hotmail.com>
 * @copyright Copyright (c) 2018, 7/6/18 12:40
 */

namespace Tests\Unit;

use Infrastructure\Product\Utils\ArrayProductInfoRender;
use Infrastructure\Product\Utils\RendererService;
use PHPUnit\Framework\TestCase;
use UseCase\GetProductInfo\ProductInfo;

class RendererServiceTest extends TestCase
{
    private $service;

    public function setUp()
    {
        $this->service = new RendererService(
            new ProductInfo(
                'Pasta de dientes',
                '901001',
                1.50,
                'Promofarma',
                0.5
            )
        );
    }

    /**
     * @test
     */
    public function itShouldReturnArrayProductInfo()
    {
        $service = new ArrayProductInfoRender($this->service);

        $expectArray = [
            'name' => 'Pasta de dientes',
            'reference' => '901001',
            'best_price' => 1.5,
            'shop' => 'Promofarma',
            'savings' => '0,5%',
        ];

        $this->assertTrue(is_array($service->render()));
        $this->assertEquals($expectArray, $service->render());
    }
}
