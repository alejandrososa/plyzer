<?php

use Controller\ProductController;
use Domain\Product\Repository\{ProductCollection as Collection, ProductRepository as Repository};
use Infrastructure\Product\Repository\{ProductCollection, ProductRepository};
use Infrastructure\Product\Service\DummyProducts;
use UseCase\GetProductInfo\{GetProductInfoService, GetProductInfoServiceImpl};

final class ServiceFactory
{
    /**
     * @param string $identifier
     * @return ProductController|Collection|ProductCollection|ProductRepository|DummyProducts|GetProductInfoService
     * @throws Exception
     */
    public static function build(string $identifier)
    {
        switch($identifier) {
            case ProductController::class:
                return self::buildProductController();
            case GetProductInfoService::class:
                return self::buildGetProductInfoService();
            case ProductCollection::class:
                return self::buildProductCollection();
            case ProductRepository::class:
                return self::buildProductRepository();
            case DummyProducts::class:
                return self::buildDummyProducts();
            default:
                throw new \Exception('Class '.$identifier.' not found');
        }
    }

    /**
     * @return ProductController
     * @throws Exception
     */
    private static function buildProductController(): ProductController
    {
        $service = self::buildGetProductInfoService();

        return new ProductController($service);
    }

    /**
     * @return GetProductInfoService
     * @throws Exception
     */
    private static function buildGetProductInfoService(): GetProductInfoService
    {
        $repository = self::buildProductRepository();

        return new GetProductInfoServiceImpl($repository);
    }

    /**
     * @return ProductRepository
     * @throws Exception
     */
    private static function buildProductRepository(): Repository
    {
        $dummyProducts = self::buildDummyProducts();

        return new ProductRepository($dummyProducts->create());
    }

    private static function buildProductCollection(): Collection
    {
        return new ProductCollection();
    }

    private static function buildDummyProducts(): DummyProducts
    {
        return new DummyProducts();
    }
}
