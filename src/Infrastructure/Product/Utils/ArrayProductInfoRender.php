<?php
/**
 * demo-search, Created by PhpStorm.
 * @author: Alejandro Sosa <alesjohnson@hotmail.com>
 * @copyright Copyright (c) 2018, 6/6/18 15:38
 */

namespace Infrastructure\Product\Utils;

use UseCase\GetProductInfo\ProductInfo;

/**
 * Class ArrayProductInfoRender
 * @package Infrastructure\Product\Utils
 */
final class ArrayProductInfoRender extends RendererDecorator
{
    /**
     * Render object to custom format
     * @return mixed
     */
    public function render()
    {
        /** @var ProductInfo $productInfo */
        $productInfo = $this->wrapped->render();
        $saving = number_format($productInfo->savings(), 1, ',', ' ');

        return [
            'name'          => $productInfo->name(),
            'reference'     => $productInfo->reference(),
            'best_price'    => sprintf('%s€', $productInfo->bestPrice()),
            'shop'          => $productInfo->bestPriceShopName(),
            'savings'       => sprintf('%s%%', $saving)
        ];
    }
}