<?php
/**
 * demo-search, Created by PhpStorm.
 * @author: Alejandro Sosa <alesjohnson@hotmail.com>
 * @copyright Copyright (c) 2018, 6/6/18 16:56
 */

namespace Infrastructure\Product\Utils;


use Domain\Product\Model\Product;
use UseCase\GetProductInfo\ProductInfo;

/**
 * Class RendererService
 * @package Infrastructure\Product\Utils
 */
final class RendererService implements Renderable
{
    /**
     * @var Product
     */
    private $productInfo;

    public function __construct(ProductInfo $product)
    {
        $this->productInfo = $product;
    }

    /**
     * Render object to custom format
     * @return string
     */
    public function render()
    {
        return $this->productInfo;
    }
}