<?php
/**
 * plyzer, Created by PhpStorm.
 * @author: Alejandro Sosa <alesjohnson@hotmail.com>
 * @copyright Copyright (c) 2018, 7/6/18 2:08
 */

namespace Infrastructure\Product\Utils;

/**
 * Class ResponseJson
 * @package Infrastructure\Product\Utils
 */
class ResponseJson
{
    private $headers;
    private $content;
    private $statusCode;
    private $protocolVersion;

    /**
     * @throws \InvalidArgumentException When the HTTP status code is not valid
     */
    public function __construct($content = null, int $status = 200, array $headers = array())
    {
        $this->headers = $headers;
        $this->content = $content;
        $this->statusCode = $status;
        $this->protocolVersion = '1.0';
    }

    public function send()
    {
        return $this->response($this->content, $this->statusCode);
    }

    private function response($data, $status = 200) {
        header("HTTP/1.1 " . $status . " " . $this->requestStatus($status));
        return json_encode($data);
    }

    private function requestStatus($code) {
        $status = array(
            200 => 'OK',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            500 => 'Internal Server Error',
        );
        return $status[$code] ?? $status[500];
    }
}