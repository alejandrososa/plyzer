<?php
/**
 * demo-search, Created by PhpStorm.
 * @author: Alejandro Sosa <alesjohnson@hotmail.com>
 * @copyright Copyright (c) 2018, 6/6/18 15:40
 */

namespace Infrastructure\Product\Utils;

/**
 * Class RendererDecorator
 * @package Infrastructure\Product\Utils
 */
abstract class RendererDecorator implements Renderable
{
    /**
     * @var Renderable
     */
    protected $wrapped;

    /**
     * RendererDecorator constructor.
     * @param Renderable $renderer
     */
    public function __construct(Renderable $renderer)
    {
        $this->wrapped = $renderer;
    }
}