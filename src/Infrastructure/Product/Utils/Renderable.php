<?php
/**
 * demo-search, Created by PhpStorm.
 * @author: Alejandro Sosa <alesjohnson@hotmail.com>
 * @copyright Copyright (c) 2018, 6/6/18 11:32
 */

namespace Infrastructure\Product\Utils;


/**
 * Interface Renderable
 * @package Infrastructure\Product\Utils
 */
interface Renderable
{
    /**
     * Render object to custom format
     * @return mixed
     */
    public function render();
}