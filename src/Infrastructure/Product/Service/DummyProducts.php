<?php
/**
 * plyzer, Created by PhpStorm.
 * @author: Alejandro Sosa <alesjohnson@hotmail.com>
 * @copyright Copyright (c) 2018, 6/6/18 2:29
 */

namespace Infrastructure\Product\Service;


use Infrastructure\Product\Factory\ProductFactory;
use Infrastructure\Product\Repository\ProductCollection;
use ServiceFactory;

/**
 * Class DummyProducts
 * @package Infrastructure\Product\Service
 */
final class DummyProducts
{
    /**
     * @return ProductCollection
     * @throws \Exception
     */
    public function create(): ProductCollection
    {
        $collection = ServiceFactory::build(ProductCollection::class);

        foreach ($this->getData() as $index => $item) {
            $collection->addProduct(
                ProductFactory::create(
                    $item['reference'],
                    $item['name'],
                    $item['price'],
                    $item['shop']
                )
            );
        }

        return $collection;
    }

    private function getData()
    {
        return [
            ['reference' => '901001', 'name' => 'Pasta de dientes', 'price' => 1.5, 'shop' => 'Promofarma'],
            ['reference' => '901001', 'name' => 'Pasta de dientes', 'price' => 2.5, 'shop' => 'Farmacia Martorell'],
            ['reference' => '901001', 'name' => 'Pasta de dientes', 'price' => 2.15, 'shop' => 'Missfarma'],
            ['reference' => '901001', 'name' => 'Pasta de dientes', 'price' => 3, 'shop' => 'Farmacia Orjales'],
            ['reference' => '901002', 'name' => 'Jarabe para la tos', 'price' => 3.6, 'shop' => 'Farmacia Vence'],
            ['reference' => '901002', 'name' => 'Jarabe para la tos', 'price' => 2.52, 'shop' => 'Openfarma'],
            ['reference' => '901002', 'name' => 'Jarabe para la tos', 'price' => 2.55, 'shop' => 'Missfarma'],
            ['reference' => '901002', 'name' => 'Jarabe para la tos', 'price' => 3.15, 'shop' => 'Farmasky'],
            ['reference' => '901002', 'name' => 'Jarabe para la tos', 'price' => 3.2, 'shop' => 'Promofarma'],
            ['reference' => '901003', 'name' => 'Pack pañales', 'price' => 16.25, 'shop' => 'Farmasky'],
            ['reference' => '901003', 'name' => 'Pack pañales', 'price' => 9.75, 'shop' => 'Openfarma'],
            ['reference' => '901003', 'name' => 'Pack pañales', 'price' => 13.5, 'shop' => 'Farmacia Martorell'],
            ['reference' => '901004', 'name' => 'Crema solar', 'price' => 6.89, 'shop' => 'Farmacia Martorell'],
            ['reference' => '901004', 'name' => 'Crema solar', 'price' => 6.08, 'shop' => 'Openfarma'],
            ['reference' => '901004', 'name' => 'Crema solar', 'price' => 7.6, 'shop' => 'Promofarma'],
        ];
    }
}