<?php
/**
 * plyzer, Created by PhpStorm.
 * @author: Alejandro Sosa <alesjohnson@hotmail.com>
 * @copyright Copyright (c) 2018, 6/6/18 0:54
 */

namespace Infrastructure\Product\Factory;


use Domain\Product\Factory\ProductFactory as Factory;
use Domain\Product\Model\Price;
use Domain\Product\Model\Product;
use Domain\Product\Model\Shop;

/**
 * Class ProductFactory
 * @package Infrastructure\Product\Factory
 */
final class ProductFactory implements Factory
{

    /**
     * Add Product to list
     * @param string $reference
     * @param string $productName
     * @param float $price
     * @param string $shopName
     * @return Product
     */
    public static function create(string $reference, string $productName, float $price, string $shopName): Product
    {
        return new Product(
            $reference,
            $productName,
            new Price($price, new Shop($shopName)
            )
        );
    }
}