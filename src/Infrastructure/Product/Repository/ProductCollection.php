<?php
/**
 * plyzer, Created by PhpStorm.
 * @author: Alejandro Sosa <alesjohnson@hotmail.com>
 * @copyright Copyright (c) 2018, 6/6/18 1:34
 */

namespace Infrastructure\Product\Repository;

use Countable;
use Iterator;
use Domain\Product\Model\Product;
use Domain\Product\Repository\ProductCollection as Collection;

/**
 * Class ProductCollection
 * @package Infrastructure\Product\Repository
 */
class ProductCollection implements Collection, Countable, Iterator
{
    /**
     * @var Product[]
     */
    private $products = [];

    /**
     * @var int
     */
    private $currentIndex = 0;

    /**
     * Add product
     * @param Product $product
     */
    public function addProduct(Product $product): void
    {
        $this->products[] = $product;
    }

    /**
     * Remove product
     * @param Product $productToRemove
     */
    public function removeProduct(Product $productToRemove): void
    {
        foreach ($this->products as $key => $product) {
            if ($product->reference() === $productToRemove->reference()) {
                unset($this->products[$key]);
            }
        }

        $this->products = array_values($this->products);
    }

    /**
     * Return the current element
     * @return Product
     */
    public function current(): Product
    {
        return $this->products[$this->currentIndex];
    }

    /**
     * Move forward to next element
     */
    public function next(): void
    {
        $this->currentIndex++;
    }

    /**
     * Return the key of the current element
     * @return mixed scalar on success, or null on failure.
     */
    public function key()
    {
        return $this->currentIndex;
    }

    /**
     * Checks if current position is valid
     * @return boolean The return value will be casted to boolean and then evaluated.
     * Returns true on success or false on failure.
     */
    public function valid(): bool
    {
        return isset($this->products[$this->currentIndex]);
    }

    /**
     * Rewind the Iterator to the first element
     */
    public function rewind(): void
    {
        $this->currentIndex = 0;
    }

    /**
     * Count elements of an object
     * @return int The custom count as an integer.
     */
    public function count(): int
    {
        return count($this->products);
    }
}