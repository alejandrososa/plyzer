<?php
/**
 * plyzer, Created by PhpStorm.
 * @author: Alejandro Sosa <alesjohnson@hotmail.com>
 * @copyright Copyright (c) 2018, 6/6/18 1:21
 */

namespace Infrastructure\Product\Repository;

use Domain\Product\Model\Product;
use Domain\Product\Repository\{ProductCollection as Collection, ProductRepository as Repository};

/**
 * Class ProductRepository
 * @package Infrastructure\Product\Repository
 */
class ProductRepository implements Repository
{
    /**
     * @var Collection
     */
    private $collection;

    /**
     * ProductRepository constructor.
     * @param Collection|null $collection
     */
    public function __construct(Collection $collection = null)
    {
        $this->collection = $collection;
    }

    /**
     * Find all products by reference
     * @param string|null $reference
     * @return array
     */
    public function findAllByReference(string $reference = null, string $orderPrice = self::DESC): array
    {
        $result = [];
        /** @var Product $item */
        foreach ($this->collection as $index => $item) {
            if($item->reference() === $reference){
                $result[] = $item;
            }
        }

        return $this->sort($result, $orderPrice);
    }

    /**
     * @return mixed
     */
    private function sort(array $products, string $order)
    {
        usort($products, function (Product $product, Product $otherProduct) use ($order) {
            if ($product->price() === $otherProduct->price()) {
                return 0;
            }

            switch ($order){
                case self::DESC:
                    $result = ($product->price() > $otherProduct->price()) ? -1 : 1;
                    break;
                case self::ASC:
                    $result = ($product->price() < $otherProduct->price()) ? -1 : 1;
                    break;
            }

            return $result;
        });
        return $products;
    }
}