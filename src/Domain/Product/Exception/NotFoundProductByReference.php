<?php
/**
 * plyzer, Created by PhpStorm.
 * @author: Alejandro Sosa <alesjohnson@hotmail.com>
 * @copyright Copyright (c) 2018, 6/6/18 0:00
 */

namespace Domain\Product\Exception;

use Exception\NotFoundException;

/**
 * Class NotFoundProductByReference
 * @package Domain\Product\Exception
 */
final class NotFoundProductByReference extends \InvalidArgumentException implements NotFoundException
{
    /**
     * @param string $reference
     * @return NotFoundProductByReference
     */
    public static function withReference(string $reference): self
    {
        return new self(sprintf('Producto con referencia "%s" no fue encontrado.', $reference));
    }
}