<?php
/**
 * plyzer, Created by PhpStorm.
 * @author: Alejandro Sosa <alesjohnson@hotmail.com>
 * @copyright Copyright (c) 2018, 5/6/18 23:34
 */

namespace Domain\Product\Model;

/**
 * Class Store
 * @package Domain\Product\Model
 */
class Shop
{
    /**
     * @var string
     */
    private $name;

    /**
     * Store constructor.
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return $this->name;
    }
}