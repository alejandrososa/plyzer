<?php
/**
 * plyzer, Created by PhpStorm.
 * @author: Alejandro Sosa <alesjohnson@hotmail.com>
 * @copyright Copyright (c) 2018, 5/6/18 23:28
 */

namespace Domain\Product\Model;

/**
 * Class Product
 * @package Domain\Product\Model
 */
class Product
{
    /**
     * @var string
     */
    private $reference;
    /**
     * @var string
     */
    private $name;
    /**
     * @var Price
     */
    private $price;

    /**
     * Product constructor.
     * @param string $reference
     * @param string $name
     * @param Price $price
     */
    public function __construct(string $reference, string $name, Price $price)
    {
        $this->reference = $reference;
        $this->name = $name;
        $this->price = $price;
    }

    /**
     * @return string
     */
    public function reference(): string
    {
        return $this->reference;
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return $this->name;
    }

    /**
     * @return Price
     */
    public function price(): Price
    {
        return $this->price;
    }
}