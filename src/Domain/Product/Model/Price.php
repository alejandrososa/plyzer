<?php
/**
 * plyzer, Created by PhpStorm.
 * @author: Alejandro Sosa <alesjohnson@hotmail.com>
 * @copyright Copyright (c) 2018, 5/6/18 23:29
 */

namespace Domain\Product\Model;

/**
 * Class Price
 * @package Domain\Product\Model
 */
class Price
{
    /**
     * @var float
     */
    private $amount;

    /**
     * @var Shop
     */
    private $shop;

    /**
     * Price constructor.
     * @param float $amount
     * @param Shop $shop
     */
    public function __construct(float $amount, Shop $shop)
    {
        $this->amount = $amount ?? 0.00;
        $this->shop = $shop;
    }

    /**
     * @return float
     */
    public function amount(): float
    {
        return $this->amount;
    }

    /**
     * @return Shop
     */
    public function shop(): Shop
    {
        return $this->shop;
    }
}