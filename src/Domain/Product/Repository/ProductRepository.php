<?php
/**
 * plyzer, Created by PhpStorm.
 * @author: Alejandro Sosa <alesjohnson@hotmail.com>
 * @copyright Copyright (c) 2018, 6/6/18 0:13
 */

namespace Domain\Product\Repository;

/**
 * Interface ProductRepository
 * @package Domain\Product\Repository
 */
interface ProductRepository
{
    const ASC   = 'asc';
    const DESC  = 'desc';

    /**
     * Find all products by reference
     * @param string|null $reference
     * @param string $order
     * @return array
     */
    public function findAllByReference(string $reference = null, string $order = self::ASC): array;
}