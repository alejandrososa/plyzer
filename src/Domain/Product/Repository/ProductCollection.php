<?php
/**
 * plyzer, Created by PhpStorm.
 * @author: Alejandro Sosa <alesjohnson@hotmail.com>
 * @copyright Copyright (c) 2018, 6/6/18 1:29
 */

namespace Domain\Product\Repository;


use Domain\Product\Model\Product;

/**
 * Interface ProductCollection
 * @package Domain\Product\Repository
 */
interface ProductCollection
{
    /**
     * Add product
     * @param Product $product
     */
    public function addProduct(Product $product): void;

    /**
     * Remove product
     * @param Product $productToRemove
     */
    public function removeProduct(Product $productToRemove): void;
}