<?php
/**
 * plyzer, Created by PhpStorm.
 * @author: Alejandro Sosa <alesjohnson@hotmail.com>
 * @copyright Copyright (c) 2018, 6/6/18 0:21
 */

namespace Domain\Product\Factory;


use Domain\Product\Model\Product;

/**
 * Interface ProductFactory
 * @package Domain\Product\Factory
 */
interface ProductFactory
{
    /**
     * Add Product to list
     * @param string $reference
     * @param string $productName
     * @param float $price
     * @param string $shopName
     * @return Product
     */
    public static function create(string $reference, string $productName, float $price, string $shopName): Product;
}