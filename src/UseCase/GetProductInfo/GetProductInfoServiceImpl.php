<?php

namespace UseCase\GetProductInfo;

use Domain\Product\Exception\NotFoundProductByReference;
use Domain\Product\Model\Product;
use Domain\Product\Repository\ProductRepository;

class GetProductInfoServiceImpl implements GetProductInfoService
{
    /**
     * @var ProductRepository
     */
    private $repository;

    /**
     * GetProductInfoServiceImpl constructor.
     * @param ProductRepository $repository
     */
    public function __construct(ProductRepository $repository)
    {
        $this->repository = $repository;
    }

    public function get(string $reference): ProductInfo
    {
        $products = $this->repository->findAllByReference($reference, ProductRepository::ASC);

        if(empty($products)){
            throw NotFoundProductByReference::withReference($reference);
        }

        /** @var Product $productCheap */
        /** @var Product $productExpensive */
        $productCheap = current($products);
        $productExpensive = end($products);

        $discount = $productExpensive->price()->amount() - $productCheap->price()->amount();
        $discountRate = (float)($discount / $productExpensive->price()->amount()) / 1;

        return new ProductInfo(
            $productCheap->name(),
            $productCheap->reference(),
            $productCheap->price()->amount(),
            $productCheap->price()->shop()->name(),
            $discountRate
        );
    }
}