<?php

namespace Controller;


use Domain\Product\Exception\NotFoundProductByReference;
use Infrastructure\Product\Utils\ArrayProductInfoRender;
use Infrastructure\Product\Utils\RendererService;
use Infrastructure\Product\Utils\ResponseJson;
use UseCase\GetProductInfo\GetProductInfoService;

final class ProductController
{
    /**
     * @var GetProductInfoService
     */
    private $service;

    /**
     * ProductController constructor.
     * @throws \Exception
     */
    public function __construct(GetProductInfoService $service)
    {
        $this->service = $service;
    }

    public function getInfoAction(string $reference)
    {
        try {
            $productInfo = $this->service->get($reference);
            $service = new RendererService($productInfo);
            $service = new ArrayProductInfoRender($service);

            $data = $service->render();
            $status = 200;
        } catch (NotFoundProductByReference $e) {
            $data =  array("message" => $e->getMessage());
            $status = 406;
        } finally {
            $response = new ResponseJson($data, $status);
            echo $response->send();
        }
    }
}
